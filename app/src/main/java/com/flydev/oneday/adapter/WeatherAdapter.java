package com.flydev.oneday.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.flydev.oneday.fragment.SingleWeatherPanel;

import java.util.List;

/**
 * Created by Modificator on 2015/2/14.
 */
public class WeatherAdapter extends FragmentStatePagerAdapter {

    List<SingleWeatherPanel> fragments;

    public WeatherAdapter(FragmentManager fm, List<SingleWeatherPanel> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        //for (SingleWeatherPanel weatherPanel : fragments)
        //   weatherPanel.initWeather();
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return view == object;
//    }
}
