package com.flydev.oneday.fragment;

import com.flydev.oneday.App;
import com.flydev.oneday.R;
import com.flydev.oneday.adapter.WeatherAdapter;
import com.flydev.oneday.utils.AssetsDatabaseManager;
import com.flydev.oneday.utils.DialogUtils;
import com.flydev.oneday.utils.WeatherAddListUtil;
import com.viewpagerindicator.CirclePageIndicator;

import android.app.Activity;
import android.app.AlertDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WeatherFragment extends Fragment implements OnClickListener {
    RelativeLayout view;
    ImageButton addWeatherIb, delWeatherIb;
    AssetsDatabaseManager databaseManager = null;
    DialogFragment dialogFragment;
    ViewPager weatherShow;
    WeatherAdapter weatherAdapter;
    List<SingleWeatherPanel> fragments;
    CirclePageIndicator pageIndicator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.weather, container, false);
        initView();
        return view;
    }

    /**
     * 初始化控件
     *
     * @param view
     */
    private void initView() {
        addWeatherIb = (ImageButton) view.findViewById(R.id.btn_add_weather);
        delWeatherIb = (ImageButton) view.findViewById(R.id.btn_del_weather);
        pageIndicator = (CirclePageIndicator) view.findViewById(R.id.circlePageIndicator);
        addWeatherIb.setOnClickListener(this);
        delWeatherIb.setOnClickListener(this);
        AssetsDatabaseManager.initManager(getActivity());
        databaseManager = AssetsDatabaseManager.getManager();
        weatherShow = (ViewPager) view.findViewById(R.id.pager_weather_show);
        fragments = new ArrayList<>();
        weatherAdapter = new WeatherAdapter(getChildFragmentManager(), fragments);
        weatherShow.setAdapter(weatherAdapter);
        pageIndicator.setViewPager(weatherShow);
        {
            SQLiteDatabase database = App.dbOpenHelper.getReadableDatabase();
            Cursor cursor = database.rawQuery("select _id,city_name,city_num from weather_inf", new String[]{});
            App.weather_Inf.clear();
            while (cursor.moveToNext()) {
                String cityName = cursor.getString(1), cityNum = cursor.getString(2);
                Log.e("WeatherFragumrnt", cityName + ":" + cityNum);
                App.weather_Inf.add(new AbstractMap.SimpleEntry<String, String>(cityName, cityNum));
                SingleWeatherPanel weatherPanel = new SingleWeatherPanel(cityNum, cityName);
                fragments.add(weatherPanel);
            }

            weatherAdapter.notifyDataSetChanged();
            database.close();
        }
        //SQLiteDatabase db1=databaseManager.getDatabase("cityid.db");
        //"http://m.weather.com.cn/data/101270101.html";
        //http://m.weather.com.cn/atad/101270101.html
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View click) {
        switch (click.getId()) {
            case R.id.btn_add_weather:
                if (fragments.size() >= 5) {
                    Toast.makeText(getActivity(), "只能添加5个", Toast.LENGTH_SHORT).show();
                    return;
                }
                RelativeLayout root = (RelativeLayout) View.inflate(getActivity(), R.layout.weather, null);
                View add = root.findViewById(R.id.panel_weather_model_add);
                root.removeView(add);
                add.setVisibility(View.VISIBLE);
                dialogFragment = DialogUtils.showDialog(this.view, add);
                add.findViewById(R.id.cancel).setOnClickListener(this);
                new WeatherAddListUtil(add, fragments, weatherAdapter, dialogFragment);
                break;
            case R.id.btn_del_weather:
                if (fragments.isEmpty()) {
                    Toast.makeText(getActivity(), "已经没有天气信息了", Toast.LENGTH_SHORT).show();
                    return;
                }
                root = (RelativeLayout) View.inflate(getActivity(), R.layout.weather, null);
                add = root.findViewById(R.id.panel_weather_model_delete);
                root.removeView(add);
                add.setVisibility(View.VISIBLE);
                dialogFragment = DialogUtils.showDialog(this.view, add);
                add.findViewById(R.id.del_weather_confirm).setOnClickListener(this);
                add.findViewById(R.id.del_weather_cancel).setOnClickListener(this);
                break;
            case R.id.cancel:
            case R.id.del_weather_cancel:
                dialogFragment.dismiss();
                break;
            case R.id.del_weather_confirm:
                SingleWeatherPanel weatherPanel = fragments.get(weatherShow.getCurrentItem());
                App.weather_Inf.remove(new AbstractMap.SimpleEntry<String, String>(weatherPanel.getCityName(), weatherPanel.getCityID()));
                SQLiteDatabase database = App.dbOpenHelper.getWritableDatabase();
                database.delete("weather_inf", "city_name=? and city_num=?", new String[]{weatherPanel.getCityName(), weatherPanel.getCityID()});
                database.close();
                fragments.remove(weatherPanel);
                weatherAdapter.notifyDataSetChanged();
                dialogFragment.dismiss();
                break;
        }

    }


}
