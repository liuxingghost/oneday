package com.flydev.oneday.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Modificator on 2015/2/2.
 */
public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table weather_inf(_id integer primary key autoincrement,city_name text,city_num text unique not null)";
        db.execSQL(sql);
        ContentValues contentValues = new ContentValues();
        contentValues.put("city_name", "北京");
        contentValues.put("city_num", "101010100");
        db.insert("weather_inf", null, contentValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //onCreate(db);
    }
}
