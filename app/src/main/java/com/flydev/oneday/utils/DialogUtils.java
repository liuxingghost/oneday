package com.flydev.oneday.utils;

import android.app.AlertDialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.flydev.oneday.fragment.BaseDialogFragment;


/**
 * Dialog工具类
 * 封装了显示Dailog的方法
 * @author boshao
 *
 */
public class DialogUtils {

    public enum DialogType{
        ADD_WEATHER,DEL_WEATHER,ADD_ALARM
    }


    public static void showAddWeatherDialog(){

    }

	/**
	 * 显示Dialog
	 * @param v
	 */
	public static DialogFragment showDialog(View v,View showView){
		FragmentActivity fActivity = (FragmentActivity) v.getContext();
		FragmentManager fm = fActivity.getSupportFragmentManager();
	    Fragment prev = fm.findFragmentByTag("dialog");
	    FragmentTransaction ft = fm.beginTransaction();
	    if (prev != null) {
	        ft.remove(prev);
	    }
	    ft.addToBackStack(null);

	    // Create and show the dialog.
	    DialogFragment newFragment = BaseDialogFragment.newInstance(showView);
	    newFragment.show(ft, "dialog");
        return newFragment;
	}
}
