package com.flydev.oneday.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.flydev.oneday.R;
import com.flydev.oneday.adapter.FragmentAdapter;
import com.flydev.oneday.view.FlowLayout;

public class HomeActivity extends BaseActivity implements
		OnClickListener, OnPageChangeListener {
	public static final String TAG = "HomeActivity";
	ViewPager vPager;
	RadioGroup rg;
	RadioButton weatherRb, newsRb, alarmRb;
	FragmentManager fm;
	FragmentTransaction ft;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.home);
		try {// 强制显示导航栏菜单键
			getWindow().addFlags(
					WindowManager.LayoutParams.class.getField(
							"FLAG_NEEDS_MENU_KEY").getInt(null));
		} catch (NoSuchFieldException e) {
			// Ignore since this field won't exist in most versions of Android
		} catch (IllegalAccessException e) {
			Log.w("feelyou.info",
					"Could not access FLAG_NEEDS_MENU_KEY in addLegacyOverflowButton()",
					e);
		}
		fm = getSupportFragmentManager();
		initView();
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		super.onAttachFragment(fragment);
	}

	private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // 透明导航栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

		rg=(RadioGroup) findViewById(R.id.rg_home);
		weatherRb = (RadioButton) findViewById(R.id.rb_weather);
		newsRb = (RadioButton) findViewById(R.id.rb_news);
		alarmRb = (RadioButton) findViewById(R.id.rb_alarm);
		vPager = (ViewPager) findViewById(R.id.vpager_home);
		findViewById(R.id.rb_weather).setOnClickListener(this);
		findViewById(R.id.rb_news).setOnClickListener(this);
		findViewById(R.id.rb_alarm).setOnClickListener(this);
		vPager.setOnPageChangeListener(this);
		vPager.setAdapter(new FragmentAdapter(fm));
        vPager.setOffscreenPageLimit(3);
		weatherRb.setChecked(true);


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		menu.findItem(R.id.about).setOnMenuItemClickListener(
				new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						new AlertDialog.Builder(HomeActivity.this,
								AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
								.setTitle("关于")
								.setMessage(
										Html.fromHtml("开发团队：<br/>" +
												"开发者：<br/>" +
												"官方社区：<a href=\"http://bbs.flydev.cc\">bbs.flydev.cc</a><br/>"))
								.setPositiveButton("确认", null).show();
						return false;
					}
				});
		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * RaidoGroup的而监听
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rb_weather:
			vPager.setCurrentItem(0);
			break;
		case R.id.rb_news:
			vPager.setCurrentItem(1);
			break;
		case R.id.rb_alarm:
			vPager.setCurrentItem(2);
			break;
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {

	}

	@Override
	public void onPageSelected(int position) {
		((RadioButton) ((LinearLayout) rg.getChildAt(0)).getChildAt(0))
				.setChecked(position==0);
		((RadioButton) ((LinearLayout) rg.getChildAt(1)).getChildAt(0))
		.setChecked(position==1);
		((RadioButton) ((LinearLayout) rg.getChildAt(2)).getChildAt(0))
		.setChecked(position==2);
	}
}
