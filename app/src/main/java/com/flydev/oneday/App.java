package com.flydev.oneday;

import android.app.Application;
import android.content.pm.PackageManager;

import com.flydev.oneday.activity.BaseActivity;
import com.flydev.oneday.utils.DBOpenHelper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by Modificator on 2014/12/15.
 */
public class App extends Application {
    public static DBOpenHelper dbOpenHelper;
    public static Set<Map.Entry<String, String>> weather_Inf = new HashSet<>();

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    void init() {
        int versionCode = 1;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        dbOpenHelper = new DBOpenHelper(getApplicationContext(), "weatherInf.db", null, versionCode);
    }

}
