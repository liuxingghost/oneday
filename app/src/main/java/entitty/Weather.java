package entitty;

import java.io.Serializable;
import java.util.Date;

public class Weather implements Serializable{
	
	private static final long serialVersionUID = 2094888676619964919L;
	
	private String weatherImgUrl;
	private int temperature;
	private Date date;
	private String province;
	private String city;
	private String area;
	private int moodIndex;
	public String getWeatherImgUrl() {
		return weatherImgUrl;
	}
	public void setWeatherImgUrl(String weatherImgUrl) {
		this.weatherImgUrl = weatherImgUrl;
	}
	public int getTemperature() {
		return temperature;
	}
	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public int getMoodIndex() {
		return moodIndex;
	}
	public void setMoodIndex(int moodIndex) {
		this.moodIndex = moodIndex;
	}
	public Weather(String weatherImgUrl, int temperature, Date date,
			String province, String city, String area, int moodIndex) {
		super();
		this.weatherImgUrl = weatherImgUrl;
		this.temperature = temperature;
		this.date = date;
		this.province = province;
		this.city = city;
		this.area = area;
		this.moodIndex = moodIndex;
	}
	public Weather() {
		super();
	}
	@Override
	public String toString() {
		return "Weather [weatherImgUrl=" + weatherImgUrl + ", temperature="
				+ temperature + ", date=" + date + ", province=" + province
				+ ", city=" + city + ", area=" + area + ", moodIndex="
				+ moodIndex + "]";
	}	
}
