package entitty;

import java.io.Serializable;
import java.util.Arrays;

public class AlarmModel implements Serializable{

	private static final long serialVersionUID = -8645495707388891174L;
	private String time;
	private String[] chooseDays;
	private boolean isCheck;
	public AlarmModel(String time, String[] chooseDays, boolean isCheck) {
		super();
		this.time = time;
		this.chooseDays = chooseDays;
		this.isCheck = isCheck;
	}
	public AlarmModel() {
		super();
	}
	
	@Override
	public String toString() {
		return "AlarmModel [time=" + time + ", chooseDays="
				+ Arrays.toString(chooseDays) + ", isCheck=" + isCheck + "]";
	}
	
}
