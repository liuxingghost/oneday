#oneday

![OSP](http://git.oschina.net/uploads/images/2015/0607/121218_2de16302_332283.png "OSP")

产品经理：吴健锋
设计师：建鑫
开发者：陈云龙

----------

资源说明： 

如您不经修改使用内置第三方和系统应用的图标资源，在标注原作者（建鑫）和原团队（LiveFACE团队）后即可使用。 

如您修改内置图标资源，与原有图标混搭使用，请发邮件获取我们的许可：f@iiiitech.com 

图片资源版权声明： 本APP所有图片资源（包括应用图标，开关素材，第三方应用图标）版权归属人：吴健锋。其本人保有一些图片资源之著作权。

----------

Google Play获取：https://play.google.com/store/apps/details?id=com.flydev.oneday

[输入图片说明](http://git.oschina.net/uploads/images/2015/0607/113131_6e9b181f_332283.jpeg "在这里输入图片标题")